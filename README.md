# Squeezing Training Data with Deep Attention Recurrent Q-Network

## Libraries
We are mainly using Lua/Torch for this project. 

* [DQN](https://github.com/kuz/DeepMind-Atari-Deep-Q-Learner)

## Main reference papers
* Deep Attention Recurrent Q-Network
* Online Batch Selection for Faster Training of Neural Networks