##Current Status
>* Add cnnGameEnv class(define in cnnGameEnv.lua) to subtitute the gameEnv in dqn.
>* Design interaction graph between CNN and DQN.
>* Determine where to pass state to DQN and where to get batch indices from DQN in CNN.(including the dp source code)
>* Determine where to get state from CNN and where to pass batch indices to CNN in DQN.(including the dp source code)

##Todo
>* Find a solution for communication between CNN and DQN(e.g. luasocket).
>* Determine message passing strategy(save in file? pass table address and then read?)

##How to run respectively
>* cd into atari/, run ```./run_cpu breakout```
>* cd into atari/cnn, run ```th train-on-mnist.lua```

##Temp files
>* temp files are saved in atari/save/
>* CLASSIFYx: store each class datapoints indices in mnist
>* ACTION: store batch indices for cnn to train(a mini-batch size is 64)
>* NEXT_STATE: store the highest layer parameters of CNN
>* LOSS: store the mini-batch loss computed by CNN

##Run
>* cd into atari/, run ```./run.sh```(you can adjust the loop times in run.sh)
