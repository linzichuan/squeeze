
local cnnGameEnv = torch.class('cnnGameEnv')

function cnnGameEnv:__init(opt)
	--add a rouletee
	--rouletee, lastloss read from file
	self.rnum = 4
	self.base = '/mnt/ramdisk/'
	self.rouletee = torch.load(self.base..'save/ROULETEE')
	if (self.rouletee == nil or self.rouletee:storage():size() ~= self.rnum) then
		local r = {}
		for i=1,self.rnum do
			r[i] = 1
		end
		self.rouletee = torch.Tensor(r)
	end
	self.lastloss = torch.load(self.base..'save/LASTLOSS') or nil
	--self.rouletee = {nil,nil,nil,nil}
	--self.lastloss = nil
	self.batchsize = 64
	print('init gameenv')
	self.c = {}
	for i=1,10 do
		self.c[i] = torch.load(self.base..'save/CLASSIFY'..i)
		--print('load classify '..i..', size = '..self.c[i]:storage():size())
	end
	torch.manualSeed(os.time())
end

function cnnGameEnv:save_temp()
	--torch.save('/home/jie/lzc/squeeze_data/atari/save/ROULETEE', self.rouletee)
	--torch.save('/home/jie/lzc/squeeze_data/atari/save/LASTLOSS', self.lastloss)
end

function cnnGameEnv:loaddata()
	--epoch_batch_indices_1.dat  epoch_classes_1.dat  epoch_loss_1.dat  epoch_state_0.dat
	--/home/jie/lzc/my-DeepMind-Atari-Deep-Q-Learner/save
	local states = torch.load('/home/jie/lzc/my-DeepMind-Atari-Deep-Q-Learner/save/epoch_state_0.dat') 
	local classes = torch.load('/home/jie/lzc/my-DeepMind-Atari-Deep-Q-Learner/save/epoch_classes_1.dat') 
	local losslist = torch.load('/home/jie/lzc/my-DeepMind-Atari-Deep-Q-Learner/save/epoch_loss_1.dat') 
	local size = classes:storage():size()
	--print(size)
	local batch_size = 64
	local batch_num = size/batch_size
	--print(batch_num)
	local actions = torch.Tensor(batch_num, 10)
	for i=1,batch_num do
		local a = batch_size*(i-1)+1
		local b = batch_size*i
		local map = torch.zeros(10)
		for j=a,b do
			map[math.floor(classes[j])] = map[math.floor(classes[j])] + 1
		end
		for j=1,10 do
			if (map[j] > 0) then
				actions[i][j] = 1
			else
				actions[i][j] = 0
			end
		end
		--print(actions[i])
	end
end

function cnnGameEnv:reward(verbose)
	verbose = verbose or false
	--compute minibatch loss and compute reward	
	local loss = torch.load(self.base..'save/LOSS')
	local reward = -1
	if (self.lastloss and loss) then
		if (self.lastloss > loss) then
			reward = math.exp(self.lastloss - loss)
			--reward = 1
		end
	end
	if (verbose) then
		print ('loss: ' .. loss)
		print ('lastloss: ' .. self.lastloss)
		print ('reward: '.. reward)
	end
	self.lastloss = loss
	return reward
end

function cnnGameEnv:getActions()
	local gameActions = {}
	for i=1,10 do
		gameActions[i] = i
	end
	return gameActions
end

function cnnGameEnv:nObsFeature()

end

function cnnGameEnv:getState(verbose) --state is set in cnn.lua
	verbose = verbose or false
	--return state, reward, term
	local tstate = torch.load(self.base..'save/NEXT_STATE')
	local size = tstate:size()[1] * tstate:size()[2]
	print(size)
	tstate = tstate:view(size)
	--[[local state = {}
	for i=1,size do
	state[i] = tstate[i]
	end]]
	local reward = self:reward(verbose)
	return tstate, reward, false
end

function cnnGameEnv:step(action, tof)
	print('step')
	io.flush()
	--subtitue rouletee[4], add action
	for i=self.rnum,2,-1 do
		self.rouletee[i] = self.rouletee[i-1]
	end
	--self.rouletee[4] = self.rouletee[3]
	--self.rouletee[3] = self.rouletee[2]
	--self.rouletee[2] = self.rouletee[1]
	self.rouletee[1] = action
	--select mini-batch according to actions
	--actions number are classes number
	local num = 0
	for i=1,self.rnum do
		if (self.rouletee[i]) then
			num = num + 1
		end
	end
	local esize = math.floor(self.batchsize / num)
	--get datapoints from /home/jie/class2index/c?.dat according to each_size
	local res = {}
	--sample self.batchsize-esize*(num-1) from class r[1]
	local c1 = self.rouletee[1]
	local r1size = self.batchsize-esize*(num-1)
	--print("r1size = "..r1size)
	--print("esize = "..esize)
	print('actions are: ')
	for i=1,self.rnum do
		print(self.rouletee[i])
	end
	io.flush()
--print (self.c[c1]:storage():size())
	local shuffle0 = torch.randperm(self.c[c1]:storage():size())
	--print (shuffle0)
	for j=1,r1size do
		--print (self.c[c1][shuffle0[j]])
		res[#res+1] = self.c[c1][shuffle0[j]]
	end
	--sample esize from class r[i]
	for i=2,self.rnum do
		if (self.rouletee[i]) then
			local c = self.rouletee[i]
			local shuffle = torch.randperm(self.c[c]:storage():size())
			for j=1,esize do
				res[#res+1] = self.c[c][shuffle[j]]
			end
		end
	end
	local batch = torch.Tensor(res) --batch_indices to learn
	--save the batch indices in ../save/ACTION for CNN
	print('finish step, write ACTION into save/ACTION')
	io.flush()
	torch.save(self.base..'save/ACTION', torch.Tensor(batch))
	torch.save(self.base..'save/ROULETEE', self.rouletee)
	torch.save(self.base..'save/LASTLOSS', self.lastloss)
	--send batch to CNN to train, and receive loss, then compute reward!
	--write CNN train interface
	return self:getState()
end

function cnnGameEnv:nextRandomGame()

end

function cnnGameEnv:newGame()

end

