
for ((e=0; e<201; e++)); do
	for ((i=0; i<390; i++)); do
		echo "epoch#"$e"--->>"$i
		cd cifar_simple;
		th train.lua -network /mnt/ramdisk/save/CIFAR -batchindex $i -epoch $e && 
		#th ./action_provider.lua 
		cd ..
		./run_gpu b 
	done
    if (($e>0&&$e%100==0)); then
		cd cifar_simple;
        th train.lua
		cd ..
    fi
done
#./reset.sh &&
#th train-on-mnist.lua

#pseudo
#for epoch from 0 to 200
#   for minibatch from 0 to 389
#       goto cifar_simple directory
#       run train.lua with arguments (&& means waiting for this command to be completed before running next command.)
#       return to last directory
#       run run_gpu with argument
#   if epoch > 0 and epoch%100 == 0
#       goto cifar_simple directory
#       run train.lua without arguments (here I think I forgot a &&?)
#       cd ..
